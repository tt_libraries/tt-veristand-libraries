# NI VeriStand Libraries by Tritem

## General information

Set of Tritem's Libraries useful for development NI VeriStand oriented products - tools, applications and Custom Devices.

## Dependency

This project depends on following libraries:

- Custom Device API.lvlib (`vi.lib\NI Veristand\Custom Device API\Custom Device API.lvlib`)
- Custom Device Offline API (`vi.lib\NI Veristand\Custom Device Offline API\Custom Device Offline API.lvlib`)
- Custom Device Utility Library (`vi.lib\NI Veristand\Custom Device Tools\Custom Device Utility Library\Custom Device Utility Library.lvlib`)
- NI_VS_Workspace ExecutionAPI.lvlib (`vi.lib\NI Veristand\Execution\Workspace\NI_VS Workspace ExecutionAPI.lvlib` )

and VeriStand dlls:

- `NationalInstruments.VeriStand`
- `NationalInstruments.VeriStand.ClientAPI`
- `NationalInstruments.VeriStand.SystemDefinitionAPI`
- `NationalInstruments.VeriStand.SystemStorage`

Note, proper _NI VeriStand_ version should be installed.

## Notes

### Automatic installation

Using _Chocolatey_ - versions up to `1.4.0`

`choco install tt-veristand-libraries -s "'https://gitlab.com/api/v4/projects/51754440/packages/nuget/v2'"`

Using _Chocolatey_ - versions from `2.0.0`

`choco install tt-veristand-libraries -s "'https://gitlab.com/api/v4/projects/51754440/packages/nuget/index.json'"`

### Manual installation

Go to `https://gitlab.com/tt_libraries/tt-veristand-libraries/-/packages` and download latest version of the package. Call

```powershell
cd <path-to-folder-with-downloaded-package>
choco install tt-veristand-libraries -s .
```

Installation script **overwrites** content of `user.lib/NI VeriStand/` folder!

Use `-f` flag to force installation procedure.

Uninstall procedure **removes** entire `user.lib/NI VeriStand/` folder!
